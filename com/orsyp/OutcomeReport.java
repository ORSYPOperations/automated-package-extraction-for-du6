package com.orsyp;

import java.util.Vector;

public class OutcomeReport {
public int nUJNodesOK=0;
public int nUJNodesKO=0;
public int nDUNodesOK=0;
public int nDUNodesKO=0;
public int nPackageExt=0;
public int nPackageNoExt=0;

public int nJobExt=0;
public int nUprocExt=0;
public int nBvExt=0;
public int nResExt=0;
public int nUClassExt=0;
public int nDomainExt=0;
public int nAppExt=0;
public int nRuleExt=0;
public int nSessExt=0;
public int nTaskExt=0;
public int nRunbookExt=0;
public int nMuExt=0;
public int nMuDepExt=0;
public int nSubAccountExt=0;
public int nQueueExt=0;
public int nCalendarExt=0;
public int nOEXExt=0;
public int nOutageExt=0;
public int nJobNoExt=0;
private int nExtraction=0;
public Vector DUNodeKO = new Vector();
public  void addnMuDepExt(int inc){nMuDepExt+=inc;}
public  void addnUprocExt(int inc){nUprocExt+=inc;}
public  void addnBvExt(int inc){nBvExt+=inc;}
public  void addnResExt(int inc){nResExt+=inc;}
public  void addnClassExt(int inc){nUClassExt+=inc;}
public  void addnDomExt(int inc){nDomainExt+=inc;}
public  void addnAppExt(int inc){nAppExt+=inc;}
public  void addnRuleExt(int inc){nRuleExt+=inc;}
public  void addnSessExt(int inc){nSessExt+=inc;}
public  void addnTskExt(int inc){nTaskExt+=inc;}
public  void addnRunbookExt(int inc){nRunbookExt+=inc;}
public  void addnMuExt(int inc){nMuExt+=inc;}
public  void addnSubAcExt(int inc){nSubAccountExt+=inc;}
public  void addnQueueExt(int inc){nQueueExt+=inc;}
public  void addnCalExt(int inc){nCalendarExt+=inc;}
public  void addnOEXcExt(int inc){nOEXExt+=inc;}
public  void addnOutageExt(int inc){nOutageExt+=inc;}


public  void addnDUNodesOK(int inc){nDUNodesOK+=inc;}
public  void addnDUNodesKO(int inc){nDUNodesKO+=inc;}	
public  void addnJobExt(int inc){nJobExt+=inc;}
public  void addnJobNoExt(int inc){nJobNoExt+=inc;}
public  void addnPackageExt(int inc){nPackageExt+=inc;}
public  void addnPackageNoExt(int inc){nPackageNoExt+=inc;}
public void addnExtraction() {nExtraction++;}
public void subnExtraction() {nExtraction--;}
public int getnExtraction() {return nExtraction;}

public void DisplayReport(){
 System.out.println("==== Execution Report ====");
 System.out.println(" +++ Number of UJ Nodes with successful connections: ["+nUJNodesOK+"]");
 System.out.println(" +++ Number of Packages successfully created: ["+nPackageExt+"]");
 //System.out.println(" +++ Number of Jobs successfully extracted: ["+nJobExt+"]");
 System.out.println(" --- Number of UJ Nodes with FAILED connections: ["+nUJNodesKO+"]");
 System.out.println(" --- Number of FAILED package creations: ["+nPackageNoExt+"]");
 System.out.println(" --- Number of Jobs FAILED Job extractions: ["+nJobNoExt+"]");
 if (!DUNodeKO.isEmpty()){
	 System.out.println(" --- Failed to reach Unijob Nodes:");
	 for(int i=0;i<DUNodeKO.size();i++){
		 System.out.println("  => "+DUNodeKO.get(i));
	 } 
 }
 System.out.println("====  End of Report  ====");
 
}

}
