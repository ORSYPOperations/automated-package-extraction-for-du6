package com.orsyp;

import java.util.Properties;

import com.orsyp.help.OnlineHelp;
import com.orsyp.util.PropertyLoader;

import edu.mscd.cs.jclo.JCLO;


class AllArgs {
    private boolean help;
    private String pwd;
    private String login;
    private int port;
    private String uvms;
    private String pckname;
    private boolean version;
    private String purge;
    private String job;
    private String node;
    private String user;
    private String target;
    private String pck;
}

public class ParametersHandler {

	public String UVMSHost;
	public int UVMSPort;
	public String UVMSLogin;
	public String UVMSPassword;
	public int SimExtractionNb;
	public boolean DisplayHelp;
	public boolean DisplayVer;
	public boolean PurgeActive;
	public int MaxNumJobPerPackage;
	
	public String PckSavePath;
	public boolean ExportPackage;
	public String RetentionDelay;
	public int NumDay;
	public int NumHour;
	public int NumMinute;
	public String DuCompany;
	public String SysPassword;
	public String SysUser;
	public String SysOS;
	
	
	public ParametersHandler(String[] argv) {
		
	String DefaultRetDelay="10:00:00";
	int DefaultNumJobPerPackage=99999999;
	String DefaultSysUser="*";
	String DefaultSysPassword="*";
	String DefaultSysOS="W32";
	
	
		  JCLO jclo = new JCLO (new AllArgs());
	        jclo.parse (argv);
	        
	        DisplayHelp=jclo.getBoolean ("help");
	        DisplayVer=jclo.getBoolean ("version");
	        UVMSPassword=jclo.getString ("pwd");
	        UVMSLogin=jclo.getString ("login");
	        UVMSHost=jclo.getString ("uvms");
	        UVMSPort=jclo.getInt ("port");
	        RetentionDelay=jclo.getString ("purge");
	        PckSavePath=jclo.getString ("pck");
	        
	        if(DisplayHelp){OnlineHelp.displayHelp(0,"Display help",Go.Release);}
	        if(DisplayVer){OnlineHelp.displayVersion(0,Go.Release,Go.APIversions);}
		    
	        // Some parameters can be empty but are mandatory.. such as password, login etc for UVMS
	        // in that case, we take into account the parameter passed, and if none, we take the default one from properties file
	        

	        Properties pGlobalVariables = PropertyLoader.loadProperties("ExtUJPackages");
		    if (UVMSPassword==null || UVMSPassword.equals("")){UVMSPassword=pGlobalVariables.getProperty("UVMS_PWD");}    
		    if (UVMSPassword==null || UVMSPassword.equals("")){OnlineHelp.displayHelp(-1,"Error: No Password Passed!",Go.Release);}
	
		    SysUser=DefaultSysUser;
		    SysPassword=DefaultSysPassword;
		    SysOS=DefaultSysOS;
		  
			PurgeActive=convertOption(pGlobalVariables.getProperty("PURGE_ACTIVE"));
			
			if(pGlobalVariables.getProperty("MAX_JOBS_PER_PACKAGE")!=null){
				MaxNumJobPerPackage=Integer.valueOf(pGlobalVariables.getProperty("MAX_JOBS_PER_PACKAGE"));
			}else{ MaxNumJobPerPackage=DefaultNumJobPerPackage;}   // consider this value as infinite..		
			if (MaxNumJobPerPackage==0){MaxNumJobPerPackage=DefaultNumJobPerPackage;}
			if (PckSavePath == null || PckSavePath.equals("")){PckSavePath=pGlobalVariables.getProperty("UVMS_EXPORT_PACKAGE_SAVE_PATH");}
			if (DuCompany == null || DuCompany.equals("")){DuCompany=pGlobalVariables.getProperty("DU_COMPANY");}
            if (UVMSLogin == null || UVMSLogin.equals("")){UVMSLogin=pGlobalVariables.getProperty("UVMS_USER");}
            if (UVMSHost == null || UVMSHost.equals("")){UVMSHost=pGlobalVariables.getProperty("UVMS_HOST");}
            ExportPackage=convertOption(pGlobalVariables.getProperty("UVMS_EXPORT_PACKAGE"));
            if (UVMSPort==0){UVMSPort=Integer.parseInt(pGlobalVariables.getProperty("UVMS_PORT"));}
            if (SimExtractionNb==0){SimExtractionNb=Integer.parseInt(pGlobalVariables.getProperty("MAXIMUM_NB_SIMULTANEOUS_EXTRACT"));}
          
            if(RetentionDelay==null||RetentionDelay==""){RetentionDelay=pGlobalVariables.getProperty("RETENTION_PERIOD");}            
            
            if (RetentionDelay.equals("") && PurgeActive){
            	System.out.println("  !!! RETENTION_PERIOD variable is missing... Default value: "+DefaultRetDelay);
            	RetentionDelay=DefaultRetDelay;}
            
            if (PurgeActive){
            String[] DateShift=RetentionDelay.split(":");
            try{
            	NumDay=Integer.parseInt(DateShift[0]);
            	NumHour=Integer.parseInt(DateShift[1]);
            	NumMinute=Integer.parseInt(DateShift[2]);
            }catch(ArrayIndexOutOfBoundsException a1){
            	System.out.println("  --- RETENTION_PERIOD variable Format is wrong... Purge is deactivated.");
            	NumDay=0;
            	NumHour=0;
            	NumMinute=0;
            	PurgeActive=false;
            }
            }

            if(UVMSHost==null||UVMSPassword==null||UVMSLogin==null||UVMSHost==""||UVMSPort==0||UVMSPassword==""||UVMSLogin==""){
            	System.out.println("-- Fatal Error in Parameters Passed [ missing login, password, hostname or port ].");
            	OnlineHelp.displayHelp(1,"Error in Parameters",Go.Release);
            }
	        
	        
	}
	public static boolean convertOption(String s){
		if (s.equalsIgnoreCase("O") || s.equalsIgnoreCase("Y")){
			return true;
		}
		return false;
	}

}
