package com.orsyp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.orsyp.util.UxDateUtil;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.central.services.INetworkNodeService;
import com.orsyp.api.deploymentpackage.PackageItem;
import com.orsyp.api.deploymentpackage.PackageList;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.comm.client.ClientServiceLocator;

/**
 * INTERNAL RELEASE NOTES:
 * 16 Sept 2011 :BSA: added multi threaded extractions & Now forces the conflict policy to "OVERWRITE"
 * Dec 5 2012: BSA: adaptation for HKBN
 * Dec 2013: BSA: adapted for UV6 (ex UV4)
 * March 2014: BSA: adapted for DU6
 *
 **/

public class Go {

	static String Customer = "ORSYP";
	static String Release="0.1";
	static String[] APIversions = {"600"};
	
	public static void main(String[] argv) throws Exception {

		System.out.println("=======================================================");
		System.out.println("  **  ORSYP Package Extraction Tool version " + Release +"    **  ");
		System.out.println("  *          ORSYP Professional Services.           * ");
		System.out.println("  * Copyright (c) 2014 ORSYP.  All rights reserved. * ");
		System.out.println("  **             Implemented for "+Customer+"              **");
		System.out.println("=======================================================");
		System.out.println("");

		OutcomeReport rep = new OutcomeReport();
				
		System.out.println("=> Loading Program Options...\n");
				
		ParametersHandler handler = new ParametersHandler(argv);
				
	    // Connecting to UVMS
	    com.orsyp.stores.UVAPIWrapper conn = new com.orsyp.stores.UVAPIWrapper(handler.UVMSHost, handler.UVMSPort, handler.UVMSLogin, handler.UVMSPassword, 
	        		   handler.DuCompany, Area.Exploitation.toString());

	    OutcomeReport Report = new OutcomeReport();
		List<NetworkNodeEntity> networkNodeList = conn.UVMSStore.Nodes.getUVNodeList(PRODUCT_TYPE.DU_OWLS);	
		List <Thread> ThreadsForExtraction = new ArrayList <Thread>();
		List <Context> AllAvailableNodeContexts = new ArrayList<Context>();
				
		for (int i = 0; i < networkNodeList.size(); i++) {
			NetworkNodeEntity ent = networkNodeList.get(i);
			if(conn.UVMSStore.Nodes.getNodeStatus(ent,PRODUCT_TYPE.DU_OWLS)!=1){
				System.out.println(" --- Cannot connect to:"+ent.getName()+".. Skipping.");
				Report.addnDUNodesKO(1);
			}else{
				System.out.println(" +++ Adding Node "+ent.getName()+" to the list of candidates for extraction.");
				Report.addnDUNodesOK(1);
				Context ctx = conn.setMainNodeContext(ent, Product.OWLS);
				AllAvailableNodeContexts.add(ctx);
			}	
		}
		
		while (AllAvailableNodeContexts.size() > 0){					
			List <Context> extArray = new ArrayList <Context>();
			for (int el=0;el<handler.SimExtractionNb;el++){
				if (!AllAvailableNodeContexts.isEmpty()){
					extArray.add(el, AllAvailableNodeContexts.get(0));
					AllAvailableNodeContexts.remove(0);
				}
			}
							
			System.out.println("\n=> Extraction(s) Started For: "+extArray.size()+" Node(s).");
							
			for (int j=0;j<extArray.size();j++){
				Context myContext = extArray.get(j);
				Environment myEnv = myContext.getEnvironment();
				String nName = myEnv.getNodeName();
				ThreadsForExtraction.add(new Du6Extraction(nName,conn,Report,handler.ExportPackage));
				ThreadsForExtraction.get(ThreadsForExtraction.size() - 1).start();
			}
			for (int k = 0; k < ThreadsForExtraction.size(); k++) {
				if (ThreadsForExtraction.get(k).isAlive()) {
					ThreadsForExtraction.get(k).join();
				}
			}
		}
		System.out.println("\n=> Extraction(s) Finished!");
		//rep.DisplayReport();
		if (handler.PurgeActive){
			System.out.println("\n=> Cleaning Packages.");
			PackageList list = conn.UVMSStore.Packages.getDu6PackageList();
			Date ExpiryDate = UxDateUtil.getShiftDate(handler.NumDay,handler.NumHour,handler.NumMinute);
					
			for (int i=0; i<list.getCount();i++){
				PackageItem item = list.get(i);
				Date LastModDate = item.getLastModificationDate();

				if (LastModDate.before(ExpiryDate)){
					conn.UVMSStore.Packages.deleteDu6Package(item);
				}
			}
					
		}
		conn.cleanup();
		System.out.println("\n=> End of Execution.");		
	}
}