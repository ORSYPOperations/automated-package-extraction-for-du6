package com.orsyp;

import java.security.Permission;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import com.orsyp.UniverseException;
import com.orsyp.api.Product;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuDependency;
import com.orsyp.api.mu.MuDependencyList;
import com.orsyp.api.mu.MuList;
import com.orsyp.api.oex.OEX;
import com.orsyp.api.oex.OEXList;
import com.orsyp.api.outage.Outage;
import com.orsyp.api.outage.OutageList;
import com.orsyp.api.resource.Resource;
import com.orsyp.api.resource.ResourceList;
import com.orsyp.api.rule.Rule;
import com.orsyp.api.rule.RuleList;
import com.orsyp.api.runbook.Runbook;
import com.orsyp.api.runbook.RunbookList;
import com.orsyp.api.session.Session;
import com.orsyp.api.session.SessionList;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskList;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.api.uprocclass.UprocClass;
import com.orsyp.api.uprocclass.UprocClassList;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserItem;
import com.orsyp.api.user.UserList;
import com.orsyp.api.application.Application;
import com.orsyp.api.application.ApplicationList;
import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.bv.BusinessViewList;
import com.orsyp.api.calendar.Calendar;
import com.orsyp.api.calendar.CalendarList;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.domain.Domain;
import com.orsyp.api.domain.DomainList;
import com.orsyp.api.dqm.DqmQueue;
import com.orsyp.api.dqm.DqmQueueList;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.central.jpa.jpo.security.PermissionEntity;
import com.orsyp.central.jpa.jpo.security.RoleEntity;
import com.orsyp.central.jpa.jpo.security.RoleType;
import com.orsyp.comm.client.ClientServiceLocator;

public class Du6Extraction extends Thread {

	private String NodenameToExtract;
	private com.orsyp.stores.UVAPIWrapper conn;
	private Package ObjectPackage;
	private OutcomeReport Report;
	private boolean ExportPackage;
	private NetworkNodeEntity NodeToExtract;
	
	public Du6Extraction(String Nodename, com.orsyp.stores.UVAPIWrapper conn, OutcomeReport report, boolean Exportpackage) throws Exception{
		// We only pass the main UVAPIWrapper to serve as a reference.. EACH invocation of the run method requires its own instance of a UVMSAPIWrapper..
		// If we were to use directly the one passed here, all concurrent runs would use the same UVMSAPIWrapper object and things would get very messy..
		this.conn = new com.orsyp.stores.UVAPIWrapper(conn.host, conn.central.getPort(), conn.central.getLogin(),conn.central.getPassword(), conn.company, conn.area);
		this.NodenameToExtract = Nodename;
		
		this.Report = report;
		this.ExportPackage=Exportpackage;
	}
	@Override
	public void run() {

		try {
			ClientServiceLocator.setContext(conn.getCentralContext());
			Collection<RoleEntity> l = this.conn.UVMSStore.Security.getUVRoleList();

			this.NodeToExtract = this.conn.UVMSStore.Nodes.getNodeEntityFromName(this.NodenameToExtract,PRODUCT_TYPE.DU_OWLS);
			
			this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
			
			// Init the final package
			String date = new SimpleDateFormat("yyyy-MM-dd-hhmmss").format(new Date());
			String PackName = this.NodenameToExtract+"_Objects_"+ date;
			String PackComment = "Package Automatically Generated";
			conn.UVMSStore.Packages.initiateDuObjectPackage(PackName,PackComment);
			// Extracting all objects
			ExtractUprocsToPackage(this.ObjectPackage);
			ExtractSessionsToPackage(this.ObjectPackage);
			ExtractResourcesToPackage(this.ObjectPackage);
			ExtractBVsToPackage(this.ObjectPackage);
			ExtractUprocClassesToPackage(this.ObjectPackage);
			ExtractDomainsToPackage(this.ObjectPackage);
			ExtractApplicationsToPackage(this.ObjectPackage);
			ExtractRulesToPackage(this.ObjectPackage);
			ExtractTasksToPackage(this.ObjectPackage);
			ExtractRunbooksToPackage(this.ObjectPackage);
			ExtractMusToPackage(this.ObjectPackage);
			ExtractMuDependenciesToPackage(this.ObjectPackage);
			ExtractUsersToPackage(this.ObjectPackage);
			ExtractCalendarsToPackage(this.ObjectPackage);
			ExtractDqmQueuesToPackage(this.ObjectPackage);
			ExtractOutagesToPackage(this.ObjectPackage);
			ExtractOEXsToPackage(this.ObjectPackage);
			
			// Finally, commit the package
			conn.UVMSStore.Packages.commitModificationsToPackage(this.ObjectPackage);

			//Report.DisplayNodeReport();
			
			String Filename = "c:\\temp\\"+this.NodenameToExtract+"_Objects"+date+".unipkg";
			if(ExportPackage){
			conn.UVMSStore.Packages.exportPackageToFile(this.ObjectPackage,Filename);
			}
		} catch (UniverseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void ExtractUprocsToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		UprocList list = this.conn.DU6Store.Uprocs.getUprocList();
		for(int i=0;i<list.getCount();i++){
			Uproc obj = conn.DU6Store.Uprocs.getUproc(list.get(i));
			conn.UVMSStore.Packages.addUprocToPackage(obj, p);
			Report.addnUprocExt(1);
		}
	}
	public void ExtractSessionsToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		SessionList list = this.conn.DU6Store.Sessions.getSessionList();
		for(int i=0;i<list.getCount();i++){
			Session obj = conn.DU6Store.Sessions.getSession(list.get(i));
			conn.UVMSStore.Packages.addSessionToPackage(obj, p);
			Report.addnSessExt(1);
		}
	}
	public void ExtractBVsToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
			BusinessViewList list = this.conn.DU6Store.BusinessViews.getBusinessViewList();
			for(int i=0;i<list.getCount();i++){
				BusinessView obj = conn.DU6Store.BusinessViews.getBusinessView(list.get(i));
				conn.UVMSStore.Packages.addBusinessViewToPackage(obj, p);
				Report.addnBvExt(1);
			}
	}
	public void ExtractResourcesToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		ResourceList list = this.conn.DU6Store.Resources.getResourceList();
		for(int i=0;i<list.getCount();i++){
			Resource obj = conn.DU6Store.Resources.getResource(list.get(i));
			conn.UVMSStore.Packages.addResourceToPackage(obj, p);
			Report.addnResExt(1);
		}
	}
	public void ExtractUprocClassesToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		UprocClassList list = this.conn.DU6Store.Uprocs.getUprocClassList();
		for(int i=0;i<list.getCount();i++){
			UprocClass obj = conn.DU6Store.Uprocs.getUprocClass(list.get(i));
			conn.UVMSStore.Packages.addUprocClassToPackage(obj, p);
			Report.addnClassExt(1);
		}
	}
	public void ExtractDomainsToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		DomainList list = this.conn.DU6Store.Domains.getDomainList();
		for(int i=0;i<list.getCount();i++){
			Domain obj = conn.DU6Store.Domains.getDomain(list.get(i));
			conn.UVMSStore.Packages.addDomainToPackage(obj, p);
			Report.addnDomExt(1);
		}
	}
	public void ExtractApplicationsToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		ApplicationList list = this.conn.DU6Store.Applications.getApplicationList();
		for(int i=0;i<list.getCount();i++){
			Application obj = conn.DU6Store.Applications.getApplication(list.get(i));
			conn.UVMSStore.Packages.addApplicationToPackage(obj, p);
			Report.addnAppExt(1);
		}
	}
	public void ExtractRulesToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		RuleList list = this.conn.DU6Store.Rules.getRuleList();
		for(int i=0;i<list.getCount();i++){
			Rule obj = conn.DU6Store.Rules.getRule(list.get(i));
			conn.UVMSStore.Packages.addRuleToPackage(obj, p);
			Report.addnRuleExt(1);
		}
	}
	public void ExtractTasksToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		TaskList list = this.conn.DU6Store.Tasks.getTaskList();
		for(int i=0;i<list.getCount();i++){
			Task obj = conn.DU6Store.Tasks.getTask(list.get(i));
			conn.UVMSStore.Packages.addTaskToPackage(obj, p);
			Report.addnTskExt(1);
		}
	}
	public void ExtractRunbooksToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		RunbookList list = this.conn.DU6Store.Runbooks.getRunbookList();
		for(int i=0;i<list.getCount();i++){
			Runbook obj = conn.DU6Store.Runbooks.getRunbook(list.get(i));
			conn.UVMSStore.Packages.addRunbookToPackage(obj, p);
			Report.addnRunbookExt(1);
		}
	}
	public void ExtractMusToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		MuList list = this.conn.DU6Store.Mus.getMuList();
		for(int i=0;i<list.getCount();i++){
			Mu obj = conn.DU6Store.Mus.getMu(list.get(i));
			conn.UVMSStore.Packages.addMuToPackage(obj, p);
			Report.addnMuExt(1);
		}
	}
	public void ExtractMuDependenciesToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		MuDependencyList list = this.conn.DU6Store.Mus.getMuDependencyList();
		for(int i=0;i<list.getCount();i++){
			MuDependency obj = conn.DU6Store.Mus.getMuDependency(list.get(i));
			conn.UVMSStore.Packages.addMuDependencyToPackage(obj, p);
			Report.addnMuDepExt(1);
		}
	}
	public void ExtractUsersToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		UserList list = this.conn.DU6Store.Users.getDu6UserList();
		for(int i=0;i<list.getCount();i++){
			User obj = conn.DU6Store.Users.getUser(list.get(i));
			conn.UVMSStore.Packages.addUserToPackage(obj, p);
			Report.addnSubAcExt(1);
		}
	}
	public void ExtractCalendarsToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		CalendarList list = this.conn.DU6Store.Calendars.getCalendarList();
		for(int i=0;i<list.getCount();i++){
			Calendar obj = conn.DU6Store.Calendars.getCalendar(list.get(i));
			conn.UVMSStore.Packages.addCalendarToPackage(obj, p);
			Report.addnCalExt(1);
		}
	}
	public void ExtractDqmQueuesToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		DqmQueueList list = this.conn.DU6Store.DqmQueues.getDqmQueueList();
		for(int i=0;i<list.getCount();i++){
			DqmQueue obj = conn.DU6Store.DqmQueues.getDqmQueue(list.get(i));
			conn.UVMSStore.Packages.addDqmQueueToPackage(obj, p);
			Report.addnQueueExt(1);
		}
	}
	public void ExtractOEXsToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		OEXList list = this.conn.DU6Store.Exceptions.getOEXList();
		for(int i=0;i<list.getCount();i++){
			OEX obj = conn.DU6Store.Exceptions.getOEX(list.get(i));
			conn.UVMSStore.Packages.addExceptionToPackage(obj, p);
			Report.addnOEXcExt(1);
		}
	}
	public void ExtractOutagesToPackage(Package p) throws Exception{
		this.conn.setMainNodeContext(this.NodeToExtract,Product.OWLS);
		OutageList list = this.conn.DU6Store.Exceptions.getOutageList();
		for(int i=0;i<list.getCount();i++){
			Outage obj = conn.DU6Store.Exceptions.getOutage(list.get(i));
			conn.UVMSStore.Packages.addOutageToPackage(obj, p);
			Report.addnOutageExt(1);
		}
	}
}
