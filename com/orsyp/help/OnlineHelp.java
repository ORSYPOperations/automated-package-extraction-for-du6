package com.orsyp.help;

public class OnlineHelp {
	public static void displayHelp(int RC, String Message, String Release) {
		
		System.out.println(Message+"\n");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("   This Tool creates one backup package per Dollar Universe 6 node");
		System.out.println("                   All Packages are stored on UVMS    ");		
		System.out.println("---------------------------------------------------------------------------\n");
		System.out.println("++ Optional Parameters:\n");
		System.out.println("    --login:     UVMS connection login     [can be specified in properties file]");
		System.out.println("    --pwd:       UVMS connection password  [can be specified in properties file]");
		System.out.println("    --uvms:      UVMS hostname             [can be specified in properties file]");
		System.out.println("    --port:      UVMS Connection port      [can be specified in properties file]");
		System.out.println("");
		System.out.println("++ Display, Help & Version Info:\n");
		System.out.println("    --help:    Display online help");
		System.out.println("    --version: Version Information & UVMS compatibility");
		System.out.println("    --verbose: Display Extra Information");
		System.out.println("");
		System.out.println("  IMPORTANT NOTE:");
		System.out.println("  => All Configuration parameters can be modified from properties file in same folder as Jar file");
		System.out.println("");
		System.out.println("++ Examples:\n");
		System.out.println("    pckExtract --pwd=universe");
		System.out.println("    pckExtract --pwd=universe --pckname=MyPackage");

		System.exit(RC);
	}

	public static void displayVersion(int RC, String release, String[] APIversions) {
		System.out.println("Version "+release);
		System.out.println("++ This Tool was compiled and tested with the following UVMS:");
		for (int j=0;j < APIversions.length;j++){
			System.out.println(APIversions[j]);
		}
		System.out.println("  => Other versions of UVMS might not work as expected or might not work at all");
		System.exit(RC);
	}
}
